#ifndef DATE_TIME_TYPES_H_INCLUDED
#define DATE_TIME_TYPES_H_INCLUDED

enum month {
    JANUARY=1, FEBRUARY, MARCH, APRIL, MAY, JUNE,
    JULY, AUGUST, SEPTEMBER, OCTOBER, NOVEMBER, DECEMBER
};

#endif // DATE_TIME_TYPES_H_INCLUDED
