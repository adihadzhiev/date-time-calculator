#ifndef DATE_TIME_FUNCTIONS_H_INCLUDED
#define DATE_TIME_FUNCTIONS_H_INCLUDED

typedef enum Boolean
{
	False = 0,
    True = 1
}

Boolean_t;

void print_date(const char separator, const unsigned short day, const unsigned month, const unsigned short year);

int get_days_count(int month, int year);

Boolean_t is_leap_year(const unsigned short year);

Boolean_t is_even(int date);

void add_days_to_date(int day, int month, int year,int add);

void subtract_days_from_date(int day, int month, int year, int subtract);

void print_month_days(int month, int year);

void increment_date(int day, int month, int year);

void decrement_date(int day, int month, int year);
#endif	// DATE_TIME_FUNCTIONS_H_INCLUDED
