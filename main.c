#include <stdio.h>
#include <stdlib.h>
#include "date_time_types.h"

void subtract_days_from_date_pointer(int* day, int* month, int* year, int subtract)
{
    *day -= subtract;
    while (*day < 1) {
        (*month)--;
        if (*month < 1) {
            (*year)--;
            *month = 12;
        }
        *day += get_days_count(*month, *year);
    }

    print_date('.', *day, *month, *year);

}


void print_adresses(int *arr,size_t elements_count)
{
    printf("The first elements is at address 0x%p\n",arr);
    printf("The middle elements is at address 0x%p\n",&arr[elements_count/2]);
    printf("The last elements is at address 0x%p\n",&arr[elements_count-1]);
}

void find_min_max(int* arr, int size, int* min_index, int* max_index)
{
    int min_val = arr[0], max_val = arr[0];

    for (int i = 1; i < size; i++) {
        if (arr[i] < min_val) {
            min_val = arr[i];
            *min_index = i;
        }

        if (arr[i] > max_val) {
            max_val = arr[i];
            *max_index = i;
        }
    }
}

void print_array(int* arr, int size) {
    printf("Forward: ");
    for (int i = 0; i < size; i++) {
        printf("%d ", arr[i]);
    }
    printf("\n");

    printf("Backward: ");
    for (int i = size - 1; i >= 0; i--) {
        printf("%d ", arr[i]);
    }
    printf("\n");
}

void sort_descending(int arr[], int size) {
    int i, j, max_index;

    for (i = 0; i < size - 1; i++) {
        max_index = i;
        for (j = i + 1; j < size; j++)
            if (arr[j] > arr[max_index])
                max_index = j;
        int temp = arr[i];
        arr[i] = arr[max_index];
        arr[max_index] = temp;
    }
}

void swap_timezone_offsets(int* addr1, int* addr2) {
    printf("Before swap: value %d at address %p; value %d at address %p\n", *addr1, addr1, *addr2, addr2);
    int temp = *addr1;
    *addr1 = *addr2;
    *addr2 = temp;
    printf("After swap: value %d at address %p; value %d at address %p\n", *addr1, addr1, *addr2, addr2);
}


int main()
{
    int arr[]={1,5,-4,3,-3,2};
    const size_t size = sizeof(arr)/sizeof(arr[0]);
    print_adresses(arr,size);

    print_array(arr,size);

    int min_index, max_index;
    find_min_max(arr, size, &min_index, &max_index);

    printf("The smallest timezone offset(%d) is with index %d at address %p\n", arr[min_index], min_index, (void*)&arr[min_index]);
    printf("The largest timezone offset(%d) is with index %d at address %p\n", arr[max_index], max_index, (void*)&arr[max_index]);

    swap_timezone_offsets(&arr[1],&arr[2]);

    sort_descending(arr,size);
    print_array(arr,size);

	return 0;

}
