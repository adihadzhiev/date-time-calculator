#include <stdio.h>
#include "date_time_functions.h"
#include "date_time_types.h"

void print_date(const char separator, const unsigned short day, const unsigned month, const unsigned short year)
{
	printf("%02hu%c%02hu%c%02hu\n", day, separator, month, separator, year);
}

Boolean_t is_leap_year(const unsigned short year)
{
	if (((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0))
		return True;
	else
		return False;
}

int get_days_count(int month, int year)
{
	if (month == FEBRUARY)
	{
		if (is_leap_year(year)) return 29;
		else return 28;
	}
	else if (month == APRIL || month == JUNE || month == SEPTEMBER || month == NOVEMBER)
		return 30;
	else if (month == JANUARY || month == MARCH || month == MAY || month == JULY || month == AUGUST || month == OCTOBER || month == DECEMBER)
		return 31;
}

Boolean_t is_even(int date)
{
	if ((date & 1) == 1) return False;
	else return True;
}



void print_month_days(int month, int year) {
    int month_days = get_days_count(month, year);
    printf("The month of ");
    switch (month) {
        case JANUARY:
            printf("January");
            break;
        case FEBRUARY:
            printf("February");
            break;
        case MARCH:
            printf("March");
            break;
        case APRIL:
            printf("April");
            break;
        case MAY:
            printf("May");
            break;
        case JUNE:
            printf("June");
            break;
        case JULY:
            printf("July");
            break;
        case AUGUST:
            printf("August");
            break;
        case SEPTEMBER:
            printf("September");
            break;
        case OCTOBER:
            printf("October");
            break;
        case NOVEMBER:
            printf("November");
            break;
        case DECEMBER:
            printf("Decembe");
            break;
        default:
            printf("Invalid month");
            break;
    }
    printf(" has %d days\n", month_days);
}

void add_days_to_date(int day, int month, int year,int add) {


	day += add;
	if (day > get_days_count(month, year))
	{
		day -= get_days_count(month, year);
		month += 1;
		if (month > 12)
		{
			month -= 12;
			year += 1;
		}
	}

     print_date('.', day, month, year);
}

void increment_date(int day, int month, int year)
{
	day += 1;
	if (day > get_days_count(month, year))
	{
		day -= get_days_count(month, year);
		month += 1;
		if (month > 12)
		{
			month -= 12;
			year += 1;
		}
	}

    print_date('.', day, month, year);
}

void subtract_days_from_date(int day,int month,int year,int subtract)
{
    day -= subtract;
    while (day < 1) {
        month--;
        if (month < 1) {
            year--;
            month = 12;
        }
        day += get_days_count(month, year);
    }

    print_date('.', day, month, year);

}

void decrement_date(int day, int month, int year)
{
    day -= 1;
    while (day < 1) {
        month--;
        if (month < 1) {
            year--;
            month = 12;
        }
        day += get_days_count(month, year);
    }

    print_date('.', day, month, year);
}
