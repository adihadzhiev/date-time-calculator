# Date Time Calculator

## Description
A program that performs various calculations with date and time values. 

## Roadmap
##### Task #1:
1.1 Read four values from the standard input - separator symbol, date, month and year; Use the most optimal (smallest) data types to store them \
1.2 Print the values on the standard output as follows: 
* The separator symbol separates the other three values
* The date and month are two characters wide 

Example input: / 1 2 2023 \
Example output: 01/02/2023


Example input: . 1 2 2023 \
Example output: 01.02.2023

1.3 Add the `printf` statement below to your `main` function:
* `printf("the date is even number: %d", <expression>);`
* Write an expression containing **bitwise** operator  to check if the given date is an even number (the expression can consist of multiple opearators)
* Write another expression **without bitwise** operator that performs the same check

Example input: / 1 2 2023 \
 Example output: the date is even number: 0

 Example input: / 10 3 2023 \
Example output: the date is even number: 1

##### Task #2:

2.1 Define an `enum` with constants for each month \
2.2 Print the values on the standard output as defined in section 1.2 but instead of a numerical value for the month, print it as a word; **Hint: use `switch` operator** 

Example input: . 1 2 2023 \
Example output: 01.February.2023

2.3 Print on the standard output how many days the month has; **Hint use `if-else if-else` statement**

Example input: . 1 2 2023 \
Example output: The month has 28 days

2.4 Write a `for` loop to print all the dates till the end of the month, each on a new line \
\
Example input: . 25 2 2023 \
Example output:  \
Upcoming detes: \
26.02.2023 \
27.02.2023 \
28.02.2023

2.5 Write a `while` or `do-while` loop to print all the dates from the beginning of the month, each on a new line \
\
Example input: . 4 2 2023 \
Example output:  
Passed detes: \
03.02.2023 \
02.02.2023 \
01.02.2023

2.6 Let 1st of July be the middle of a year and beginning of its second half; Print all the remaining days up to: 
* 1st of July if the month entered is in the first half of the year
* 31st of December if the month entered is in the second half of the year

Print each date on a new line; The month is printed as numerical value; **Hint: use nested loops**

## Coding guidelines
* Use "snake case" for variable and function names (e.g `foo_value` instead of `fooValue`)
* Do NOT start your variable names with '_'
* Do NOT name your variables with a single letter (e.g 'd' intstead of 'date', 'm' isntead of 'month', etc)
* Avoid magic numbers (use `const`ants, `enum`arators, or `#define`s)
* Use lower case for file names

## Commit message rules
Each commit message must be in the format: *"Task#X: Descriptive information what has changed"*, where X is the task number

Example commit messages for changes related to Task#1:
* *"Task#1: Added reading of date, month, year"*
* *"Task#1: Correction of the print format"*

